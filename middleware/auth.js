const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcrypt');
const User = require('../database/db_operations/users-db.js');

passport.use(new LocalStrategy(
  {
    usernameField: 'email',
  },
  function(username, password, done) {
    User.findByEmail(username).then((user) => {
      if (!user) {
        return done(null, false, { message: 'Unknown User' });
      }
      if (user) {
        bcrypt.compare(password, user.password.toString(), function(err, res) {
          if (res) {
            return done(null, user);
          }
          if (err) {
            return done(null, false, { message: 'Incorrect password.' });
          }
          return done(null, false);
        });
      }
    }).catch((err) => {
      if (err) { return done(err); }
    });
  }
));
