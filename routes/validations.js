const Joi = require('joi');

module.exports = {
  // Validate Category while adding or updating
  validCategory(category) {
    const hasCategoryId = typeof category.category_id_pk === 'string' && category.category_id_pk.trim() !== '';
    const hasCategory = typeof category.category_name === 'string' && category.category_name.trim() !== '';
    return hasCategoryId && hasCategory;
  },
  // Validate Member while adding or updating
  validMember(member) {
    const hasMemberId = typeof member.member_id_pk === 'string' && member.member_id_pk.trim() !== '';
    const hasName = typeof member.member_name === 'string' && member.member_name.trim() !== '';
    const hasEmailId = typeof member.member_email_id === 'string' && member.member_email_id.trim() !== '';
    const hasPhoneNo = typeof member.member_phone_no === 'string' && member.member_phone_no.length === 10;
    const hasAddress = typeof member.member_address === 'string' && member.member_address.trim() !== '';
    const hasState = typeof member.member_state === 'string' && member.member_state.trim() !== '';
    const hasCountry = typeof member.member_country === 'string' && member.member_country.trim() !== '';
    const hasZipCode = typeof member.member_zip_code === 'string' && member.member_zip_code.trim() !== '';
    return hasMemberId && hasName && hasEmailId && hasPhoneNo && hasAddress && hasState && hasCountry && hasZipCode;
  },
  // Validate Group while adding or updating
  validGroup(group) {
    const hasGroupId = typeof group.group_id_pk === 'string' && group.group_id_pk.trim() !== '';
    const hasGroupName = typeof group.group_name === 'string' && group.group_name.trim() !== '';
    const hasGroupMembers = typeof group.group_members === 'string' && group.group_members.trim() !== '';
    const hasGroupType = typeof group.group_type === 'string' && group.group_type.trim() !== '';
    const hasCategoryId = typeof group.category_id_fk === 'string' && group.category_id_fk.trim() !== '' && group.category_id_fk.length < 3;
    const hasDescription = typeof group.group_description === 'string' && group.group_description.trim() !== '';
    return hasGroupId && hasGroupName && hasGroupMembers && hasGroupType && hasCategoryId && hasDescription;
  },
  // Validate Event while adding or updating
  validEvent(event) {
    const hasEventId = typeof event.event_id_pk === 'string' && event.event_id_pk.trim() !== '';
    const hasEventName = typeof event.event_name === 'string' && event.event_name.trim() !== '';
    const hasDescription = typeof event.event_description === 'string' && event.event_description.trim() !== '';
    const hasEventDuration = typeof event.event_duration === 'string' && event.event_duration.trim() !== '';
    const hasGroupId = typeof event.group_id_fk === 'string' && event.group_id_fk.trim() !== '';
    const hasDate = typeof event.event_date === 'string' && event.event_date.trim() !== '';
    const hasMemberId = typeof event.member_id === 'string' && event.member_id.trim() !== '';
    return hasEventId && hasEventName && hasDescription && hasEventDuration && hasGroupId && hasDate && hasMemberId;
  },
  validateUser(user){
    const validateNewUser = Joi.object().keys({
      member_name: Joi.string()
        .required(),
      email: Joi.string().email()
        .required(),
      password: Joi.string()
        .required(),
    });

    return Joi.validate(user, validateNewUser);
  },
}
