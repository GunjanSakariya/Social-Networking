var express = require('express');
var router = express.Router();
const passport = require('passport');
require('../middleware/auth.js');
const { validateUser } = require('./validations');
const bcrypt = require('bcrypt');
const user = require('../database/db_operations/users-db.js');
const saltRounds = 10;

// To display login page
router.get('/login', function(req, res) {
  res.render('login');
});

// To redirect to index page if user is authenticated
router.post(
  '/login', passport.authenticate('local', { successRedirect: '/index', failureRedirect: '/login', failureFlash: 'Invalid username or password.' }),
  function(req, res) {
    res.redirect('index');
  }
);

// To display signup page
router.get('/signup', function(req, res) {
  res.render('signup');
});

// To redirect to login page if User is registered successfully
router.post('/signup', function(req, res) {
  const member_name = req.body.member_name;
  const email = req.body.email;
  const password = req.body.password;

  const { error } = validateUser({ member_name: member_name, email: email, password: password });
  if (error === null) {
    bcrypt.hash(password, saltRounds).then(function(hash) {
      user.createLocalUser(email, hash).then((newUser) => {
        if (newUser) {
          user.createMemberViaSignup(member_name, email).then((member) => {
            console.log(member[0].member_id_pk);
            if (member[0].member_id_pk) {
              req.login(member[0].member_id_pk, function() {
                req.flash('user_msg', 'Successfully signed up please login.');
                res.redirect('/user/login');
              });
            }
          }).catch((err) => {
            throw err;
          });
        }
      }).catch((error) => {
        res.render('signup', {
          message: error,
        });
      });
    }).catch(() => {
      res.render('signup', {
        message: 'Error Occured during Sign up',
      });
    });
  } else {
    res.render('signup', {
      message: 'validation failed',
    });
  }
});

passport.serializeUser(function(member_id, done) {
  done(null, member_id);
});

passport.deserializeUser(function(member_id, done) {
  user.findUserById(member_id).then(() => {
    done(null, member_id);
  });
});

module.exports = router;
