const knex = require('./connection');
const strftime = require('strftime');

module.exports = {
  getAllEvents() {
    return knex('events');
  },
  getEventsByOffset(limit, offset) {
    return knex('events').limit(limit).offset(offset);
  },
  getEvent(id){
    return knex('events').where('event_id_pk', id).first();
  },
  createEvent(event) {
    const currentTimestamp = strftime('%F %T', new Date())
    event.event_created_date = currentTimestamp;
    event.event_status = 'upcoming';
    return knex('events').insert(event, '*');
  },
  updateEvent(id, event){
    const currentTimestamp = strftime('%F %T', new Date());
    event.event_updated_date = currentTimestamp;
    return knex('events').where('event_id_pk', id).update(event, '*');
  },
  deleteEvent(id) {
    const currentTimestamp = strftime('%F %T', new Date());
    return knex('events').where('event_id_pk', id).update({
      event_deleted_date: currentTimestamp,
      event_status: 'deleted'
    }, '*');
  },
  patchEvent(id, event) {
    return knex('events').where('event_id_pk', id).update({
      event_name: event.event_name
    }, '*');
  }
}
